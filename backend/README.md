## Install requirements

```bash
$ pip install -r requirements/development.txt
```

## Run a database

```bash
$ docker-compose up -d postgres
```

## Apply migrations

```bash
$ python3 ./manage.py migrate
```

## Also you need to specify credentials

```bash
$ inv set-secrets vk-username vk-password admin-email admin-password
```

You can specify only one of credentials
```bash
$ inv update-admin-secrets admin-email admin-password
```

```bash
$ inv update-vk-secrets vk-username vk-password
```

## Run server

```bash
$ python3 ./manage.py runserver
```

## Run application in Docker

Install invoke before
```bash
$ pip install invoke
```

Prepare and build application with the fake data.:
```bash
$ inv prepare --seed
```
*NOTE: to reset database use flag -f or --fresh.*

Build the application:
```bash
$ inv build
```

Run the application:
```bash
$ inv run
```

Stop the application:
```bash
$ inv stop
```

Stop and remove the application:
```bash
$ inv destroy
```

Run the application tests:
```bash
$ inv test
```

## Run Celery(Without a docker)

Full workflow requires worker and scheduler

```bash
$ celery --app config.celery:app worker -l info
```

```bash
 $ celery --app config.celery:app beat -l info -S django
```