from rest_framework import routers
from rest_framework.urls import path
from apps.events.api.views import EventViewSet, CalendarViewSet

app_name = 'events.api'

router = routers.SimpleRouter()
router.register('events-api', EventViewSet, basename='events')
router.register('calendar-api', CalendarViewSet, basename='calendar')

urlpatterns = router.urls
