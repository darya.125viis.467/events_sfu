import axios from "axios";
import * as API_URLS from "./constants"

class PasswordResetService {
  resetResponse(email) {
    return axios
      .post(API_URLS.USER_API_URL + "password/reset/", {
        "email": email,
      })
      .then(response => {
        return response.data;
      });
  }

  reset(uid, token, new_password1, new_password2) {
      return axios
          .post(API_URLS.USER_API_URL + "password/reset/confirm/", {
              "uid": uid,
              "token": token,
              "new_password1": new_password1,
              "new_password2": new_password2,
          })
          .then(response => {
            return response.data;
          });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));
  }
}

export default new PasswordResetService();