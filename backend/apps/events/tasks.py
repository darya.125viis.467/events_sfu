import vk_api
from apps.events.models import Organization
from apps.events.parsertools.parser import parse
from config.celery import app
from django.conf import settings


def get_vk_access_token() -> str:
    vk_session = vk_api.VkApi(
        login=settings.SECRETS.get('VK_USERNAME', None),
        password=settings.SECRETS.get('VK_PASSWORD', None),
    )
    vk_session.auth(token_only=True)
    return vk_session.token.get('access_token')


@app.task
def parse_events(wall_name, wall_id, access_token):
    parse(wall_name, wall_id, access_token)


@app.task
def parse_all_events():
    organizations = Organization.objects.all().values_list('id', 'name')
    vk_tools = get_vk_access_token()
    for wall_id, wall_name in organizations:
        parse_events.delay(wall_name, wall_id, vk_tools)
