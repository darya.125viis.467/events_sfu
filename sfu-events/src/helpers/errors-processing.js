export function get_errors(error_dict){
    return Object.keys(error_dict).map(function(key){
                return error_dict[key];
            });
}