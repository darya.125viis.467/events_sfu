import uuid

from django.core.validators import MinValueValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class Events(models.Model):
    """User event model.

    Attributes:
        owner (Manager): Foreign key User that created this event.
        title (str): Event title.
        place (bool): Location of the event.
        created (datetime): Date when the event was created.
        start (datetime): Date when the event starts.
        description (set): Event description.

    Nested attributes:
        posts (Manager): Posts that belongs to this event.

    """
    owner = models.ForeignKey(
        to='users.User',
        on_delete=models.CASCADE,
        related_name='own_events',
        verbose_name=_('Event_id'),
    )
    post_id = models.IntegerField(
        verbose_name=_('Post_id'),
        validators=[MinValueValidator(0), ],
    )
    organization = models.ForeignKey(
        to='events.Organization',
        on_delete=models.CASCADE,
        related_name='events',
        verbose_name=_('Organization'),
    )
    name = models.CharField(
        max_length=100,
        verbose_name=_('Name'),
    )
    description = models.TextField(
        max_length=10000,
        verbose_name=_('Description'),
    )
    date = models.DateTimeField(
        verbose_name=_('Date'),
        default=timezone.now
    )
    location = models.CharField(
        max_length=250,
        verbose_name=_('Location'),
        blank=True,
        default=''
    )
    review = models.CharField(
        max_length=250,
        verbose_name=_('Review'),
        default=''
    )
    unique_folder_name = models.UUIDField(
        verbose_name=_('Unique folder name'),
        blank=True,
        unique=True,
        default=uuid.uuid4,
    )
    image = models.URLField(
        max_length=500,
        verbose_name=_('Image url'),
        default="http://localhost:8000/media/logo.jpg"
    )

    class Meta:
        ordering = ('date',)
        unique_together = ('post_id', 'organization')


class Organization(models.Model):
    id = models.IntegerField(
        primary_key=True,
        auto_created=False,
    )
    name = models.CharField(
        max_length=100,
        verbose_name=_('Name'),
    )
    institute = models.CharField(
        max_length=100,
        verbose_name=_('Institute'),
        default='',
    )

    def __str__(self):
        return f'{self.name}'


class Activity(models.Model):
    name = models.CharField(
        max_length=100,
        verbose_name=_('Name'),
    )
    organizations = models.ManyToManyField(
        to='events.Organization',
        through='events.OrganizationAndActivity',
        related_name='activities',
    )

    def __str__(self):
        return f'{self.name}'


class OrganizationAndActivity(models.Model):
    organization = models.ForeignKey(
        to='events.Organization',
        on_delete=models.CASCADE,
    )
    activity = models.ForeignKey(
        to='events.Activity',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f'{self.organization.name}: {self.activity.name.upper()}'
