from django.core.management import call_command
from django.core.serializers import python, base
from django.db import migrations


def load_fixture(apps, schema_editor):
    """Load initial organization data to the database.

    Save the old _get_model() function and define new _get_model() function,
    which utilizes the apps argument to get the historical version of a model.
    """
    old_get_model = python._get_model

    def _get_model(model_identifier):
        try:
            return apps.get_model(model_identifier)
        except (LookupError, TypeError):
            raise base.DeserializationError(
                f'Invalid model identifier: "{model_identifier}"'
            )

    # Replace the _get_model() function on the module, so loaddata can
    # utilize it.
    python._get_model = _get_model

    try:
        call_command('loaddata', 'organization_data.json', app_label='events')
    finally:
        # Restore old _get_model() function
        python._get_model = old_get_model


class Migration(migrations.Migration):
    dependencies = [
        ('events', '0002_auto_20210217_2004'),
    ]

    operations = [
        migrations.RunPython(load_fixture),
    ]
