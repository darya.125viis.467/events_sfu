import axios from "axios";
import * as API_URLS from "./constants"

class AuthService {
  login(email, password) {
    return axios
      .post(API_URLS.USER_API_URL + "token/", {
        "email": email,
        "password": password,
      })
      .then(response => {
        if (response.data.access) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(username, email, password) {
    return axios.post(API_URLS.USER_API_URL + "registration/", {
      "username": username,
      "email": email,
      "password": password
    });
  }
}

export default new AuthService();