from apps.events.tasks import parse_all_events


def run():
    """Script for async parsing all organizations."""
    parse_all_events.delay()
