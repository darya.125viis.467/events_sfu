from django import forms

from apps.events.models import Organization, Activity


class OrganizationForm(forms.ModelForm):
    activities = forms.ModelMultipleChoiceField(
        queryset=Activity.objects.all(),
        required=False,
    )

    class Meta:
        model = Organization
        fields = ('id', 'name', 'institute', 'activities')
