from __future__ import absolute_import, unicode_literals
import os
import socket
from datetime import timedelta

from celery import Celery
from django.conf import settings
from kombu import Queue

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.base')

app = Celery(
    main='events-sfu',
    backend=settings.CELERY_BACKEND,
    broker=settings.CELERY_BROKER,
)

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.task_queues = (
    Queue(settings.CELERY_TASK_DEFAULT_QUEUE),
    Queue(socket.gethostname(), expires=86400),
)

app.conf.beat_schedule = {
    'ParseNewEvents': {
        'task': 'apps.events.tasks.parse_all_events',
        'schedule': timedelta(hours=1),
    }
}
