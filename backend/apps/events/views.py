from django.http import HttpResponse
from django.shortcuts import render
from apps.events.parsertools.parser import parse

from apps.events.models import Events, Organization
from apps.events.tasks import get_vk_access_token

orgz_sfu_ids = {
    77101204: 'Клуб интеллектуальных игр СФУ',
    2558401: 'Центр студенческой культуры СФУ',
    66921638: 'Совет обучающихся СФУ',
    411321: 'ППОС СФУ (Профком студентов)',
    7091516: 'Рок-клуб СФУ',
    171811692: 'Психологическая служба СФУ',
    52332777: 'Новости СФУ',
    220664: 'Сибирский федеральный университет',
    57465803: 'Научная библиотека СФУ',
    43611414: 'Волонтерский центр СФУ',
    22271849: 'Центр карьеры СФУ',
    75035840: 'НОЦ Молодых ученых СФУ',
    43160394: 'Японский центр СФУ',
    103979156: 'Французский центр СФУ',
    3321665: 'Студенческие отряды СФУ',
    36938132: 'Союз молодежи СФУ',
    49895902: 'Антикоррупционный студенческий клуб',
    84910757: 'Беги за мной! СФУ',
}

orgz_ikit_ids = {
    129406083: 'Научный клуб МЦ ИКИТ',
    2289674: 'ПОС ИКИТ (профсоюз)',
    101721701: 'МЦ ИКИТ СФУ',
    116131349: 'Информер ИКИТ СФУ',
    62073707: 'Абитуриент ИКИТ СФУ',
    81840795: 'Microsoft IT Academy ИКИТ',
    174215879: 'Учебно-организационный отдел ИКИТ'
}

orgz_pi_ids = {30617342: 'Информер ПИ СФУ'}

orgz_dorms_ids = {
    58505060: 'вОбщежитии СФУ',
    172757870: 'Общежитие СФУ №30',
    170966602: 'Общежитие СФУ №5',
    186632894: 'Общежитие СФУ №21 (Tesla Village)',
    38613202: 'Общежитие СФУ №22'
}

all_ids = {**orgz_sfu_ids, **orgz_ikit_ids, **orgz_pi_ids, **orgz_dorms_ids}


def parseView(request, wall_id=None):
    access_token = get_vk_access_token()
    if wall_id:
        parse(all_ids[wall_id], wall_id, access_token)
    else:
        organizations = Organization.objects.all().values_list('id', 'name')
        for wall_id, wall_name in organizations:
            print(wall_name)
            parse(wall_name, wall_id, access_token)

    return HttpResponse("Картинки спарсились")
