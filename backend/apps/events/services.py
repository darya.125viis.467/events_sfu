def post_image_path(instance, filename):
    """Get filename for event post image.

    File will be uploaded to
    MEDIA_ROOT/events/event-event_id/filename
    """
    event = instance.event_post.event
    return f'events/{event.unique_folder_name}/images/{filename}'