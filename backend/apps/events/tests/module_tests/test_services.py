from django.test import TestCase

from apps.events.services import post_image_path
from apps.events.tests.factories import EventPostImageFactory


class PostImagePathTestCase(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_post_image_path(self):
        file_name = 'some_file_name.jpeg'
        event_post_image = EventPostImageFactory.create()
        event = event_post_image.event_post.event
        result = post_image_path(instance=event_post_image, filename=file_name)
        self.assertEqual(
            result,
            f"events/{event.unique_folder_name}/images/{file_name}"
        )
