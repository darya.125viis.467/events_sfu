from django.urls import path
from .views import parseView

app_name = 'events'

urlpatterns = [
    path('parse/<int:wall_id>/', parseView, name='parse_with_id'),
    path('parse/all/', parseView, name='parse_all'),
]
