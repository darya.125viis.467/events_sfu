from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import User

__all__ = (
    'UserAdmin',
)


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """User admin.

    Admin class definitions for ``User`` model.

    """
    search_fields = ('username', 'email')
    list_display = (
        'id',
        'email',
        'created_at',
        'is_active',
        'is_staff',
        'is_superuser'
    )
    list_display_links = ('email',)
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {
            'fields': (
                'username',
            )
        }),
        (_('Permissions'), {
            'fields': (
                'is_active',
                'is_staff',
                'is_superuser',
                'groups',
                'user_permissions'
            )
        }),
        (_('Important dates'), {
            'fields': ('last_login',)
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    readonly_fields = DjangoUserAdmin.readonly_fields
    ordering = ('email',)
    form = CustomUserChangeForm
    add_form = CustomUserCreationForm
