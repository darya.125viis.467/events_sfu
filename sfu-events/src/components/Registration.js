import React, { Component } from 'react';
import styles from '../styles/styles.css';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import AuthService from "../services/auth.service";
import {email, required, vpassword, vusername, vconfirm} from "../helpers/validators";
import {get_errors} from "../helpers/errors-processing";

class Registration extends Component {
  constructor (props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.state = {
      username: "",
      email: "",
      password: "",
      successful: false,
      message: "",
    }
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleRegister(e) {
    e.preventDefault();

    this.setState({
      message: "",
      successful: false
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      AuthService.register(
        this.state.username,
        this.state.email,
        this.state.password
      ).then(
        response => {
            this.props.history.push("/signin");
            window.location.reload();
        },
        error => {
            const error_dict = error.response.data;
            let values = get_errors(error_dict)

            this.setState({
                loading: false,
                message: values
            });
        }
      );
    }
  }

  render() {
    return (
      <div>
        <img src="../images/background.png"
             className="background-image"/>
        <Form className="form-registration"  onSubmit={this.handleRegister}
                                              ref={c => {
                                              this.form = c;
                                                        }}>
          {!this.state.successful && (<div>
            <div className="form-group">
              <Input className="input-login"
                   type="email"
                   name="email"
                   placeholder="Aдрес электронной почты"
                   validations={[required, email]}
                   value={this.state.email}
                   onChange={this.onChangeEmail}
              />
            </div>
            <div className="form-group">
              <Input className="input-username"
                   type="username"
                   name="text"
                   value={this.state.username}
                   validations={[required, vusername]}
                   onChange={this.onChangeUsername}
                   placeholder="Имя пользователя"
                   minLength="3"
                   maxLength="20"
              />
            </div>
            <div className="form-group">
              <Input className="input-password"
                   type="password"
                   name="password"
                   placeholder="Пароль"
                   value={this.state.password}
                   onChange={this.onChangePassword}
                   validations={[required, vpassword]}
                   minLength="8"
                   maxLength="40"
              />
            </div>
            <div className="form-group">
              <Input className="input-password"
                   type="password"
                   name="confirm"
                   placeholder="Повторите пароль"
                   validations={[required, vpassword, vconfirm]}
                   minLength="8"
                   maxLength="40"
              />
            </div>
            <div className="form-group">
              <button className="sign-up" >Зарегистрироваться</button>
            </div>
          </div>)}
          {this.state.message && (
              <div className="form-group">
                <div
                  className={
                    this.state.successful
                      ? "alert alert-success"
                      : "alert alert-danger"
                  }
                  role="alert"
                >
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
        </Form>
      </div>
    );
  }

}


export default Registration;
