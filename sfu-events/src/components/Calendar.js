import React, { Component } from 'react';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/common/main.css';
import UserService from '../services/user.service'
import '../styles/styles.css';
import Modal from "./Modal";
import EventWindow from "./EventWindow";

class Calendar extends Component {
  constructor() {
    super();
    this.openWindow = this.openWindow.bind(this);
    this.calendar = React.createRef();
    this.state = {
      userEvents: [],
      openedEvent: null,
      isOpen: false,
      blockModalWindow: false,
    }
  }

  getUserEventsForThisMonth(info, successCallback, failureCallback) {
        let parsedEvents = [];
        UserService.getUsersEvents(info.startStr, info.endStr)
            .then(function (result) {
                let events = result.data;
                events.forEach(function (item, index, array) {
                    parsedEvents.push(
                        {
                            'title': item.name,
                            'date': item.date,
                            'event_instance': item,
                            'className': 'Event'
                        }
                    );
                })
                successCallback(parsedEvents)
            }.bind(this))
            .catch(reason => failureCallback(reason))
  }

  blockWindow = () => {this.setState({blockModalWindow: true})}
  unlockWindow = () => {this.setState({blockModalWindow: false})}
  getBlockWindow = () => {return this.state.blockModalWindow}

  openWindow(eventInfo){
        if (this.getBlockWindow() === false) {
            this.setState({isOpen: true, openedEvent: eventInfo.event.extendedProps.event_instance});
            this.blockWindow();
        }
  }

  closeWindow = () => {
    this.setState({isOpen: false});
    this.unlockWindow();
    this.render();
  }

  renderEventContent(eventInfo) {
      return (
        <>
            <div className="fc-daygrid-event-dot"></div>
            <div className="fc-event-title">{eventInfo.event.title}</div>
        </>
      )
 }

  render() {
    return(
      <div className="calendar-container">
        <FullCalendar ref={this.calendar}
          plugins={[ dayGridPlugin ]}
          initialView="dayGridMonth"
          locale="ru"
          firstDay="1"
          stickyHeaderDates={true}
          height="600px"
          windowResizeDelay="0"
          events={this.getUserEventsForThisMonth}
          eventContent={this.renderEventContent}
          headerToolbar={
            {start: 'prev',
            center: 'title',
            end: 'next'}
          }
          eventClick={this.openWindow}
        />
      <div>
          {(this.state.isOpen && <Modal> <EventWindow event={this.state.openedEvent} close={this}/></Modal>)}
      </div>
      </div>
    );
  }
}
export default Calendar;