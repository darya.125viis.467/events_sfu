from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import timezone

from apps.events.validators import events_start_date_validator, LessThanCreationDateException


class ValidatorsTestCase(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_events_start_date_validator_wrong_input(self):
        wrong_input_date = timezone.now() - timezone.timedelta(1)
        msg = 'Validator must raise LessThanCreationDateException'
        with self.assertRaises(LessThanCreationDateException, msg=msg):
            events_start_date_validator(wrong_input_date)

    def test_events_start_date_validator_correct_input(self):
        correct_input_date = timezone.now() + timezone.timedelta(1)
        try:
            result = events_start_date_validator(correct_input_date)
            self.assertIsNone(result)
        except ValidationError as e:
            self.fail(f'Validator raised {e.message}')
