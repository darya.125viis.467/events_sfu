from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework import viewsets, mixins

from apps.events.api.exceptions import ReadOnlyError
from apps.events.api.serializers import EventSerializer
from apps.events.models import Events

from .filters import EventFilter


class EventViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing the events
    """
    serializer_class = EventSerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        # return Events.objects.filter(description__regex=r'#анонс@chgk_sfu')
        # return Events.objects.filter(organization__in=['Клуб интеллектуальных игр СФУ'])
        return Events.objects.all().order_by('-date')

    @action(detail=False, methods=['post'])
    def get_events_by_tag(self, request, *args, **kwargs):
        tag = request.POST.get('tag', None)
        if tag:
            events = Events.objects.filter(text__regex=r'' + tag)
            serializer = self.get_serializer(events, many=True)
            return Response(serializer.data)
        else:
            return Response({'error': 'tag is required'},
                            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def get_events_by_date(self, request, *args, **kwargs):
        date = request.POST.get('date', None)
        if date:
            events = Events.objects.filter(date__day=date.day, date__month=date.month, date__year=date.year, )
            serializer = self.get_serializer(events, many=True)
            return Response(serializer.data)
        else:
            return Response({'error': 'date is required'},
                            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def get_filtered_events(self, request, *args, **kwargs):
        filter_data = {}

        if "activities" in request.data:
            activities = request.data.get('activities', None)
            if activities:
                filter_data['organization__activities__name__in'] = activities

        if "institutions" in request.data:
            institutions = request.data.get('institutions', None)
            if institutions:
                filter_data['organization__institute__in'] = institutions

        if "organizations" in request.data:
            organizations = request.data.get('organizations', None)
            if organizations:
                filter_data['organization__name__in'] = organizations

        if filter_data:
            events = Events.objects.filter(**filter_data)
        else:
            events = Events.objects.all()

        serializer = self.get_serializer(events, many=True)
        return Response(serializer.data)

    def _get_read_only_fields(self):
        return self.get_serializer().Meta.read_only_fields

    def _check_read_only_fields_in_request(self, request):
        for read_only_field in self._get_read_only_fields():
            if read_only_field in request.data:
                raise ReadOnlyError(f'{read_only_field} is read only')

    @action(detail=True, methods=['PATCH'], url_path='join')
    def join(self, request, *args, **kwargs):
        """Позволяет присоединить пользователя к событию."""
        event = self.get_object()
        event.users.add(self.request.user)
        event.save()
        serializer = self.get_serializer(event)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        self._check_read_only_fields_in_request(request)
        return super(EventViewSet, self).update(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        self._check_read_only_fields_in_request(request)
        return super(EventViewSet, self).create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class CalendarViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet
):
    """Viewset for user calendar."""
    serializer_class = EventSerializer
    lookup_field = 'id'
    filter_class = EventFilter

    def get_queryset(self):
        """Return only user's events."""
        queryset = self.request.user.events.all()

        return queryset

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_200_OK, data={"detail": "Deleted"})

    def perform_destroy(self, instance):
        """Remove event from many-to-many relationship."""
        instance.users.remove(self.request.user)
