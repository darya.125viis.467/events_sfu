import React, { Component } from 'react';
import Categories from './Categories';
import EventCard from './EventCard';
import styles from '../styles/styles.css';
import Footer from "./Footer";
import ReactDOM from 'react-dom';

class Events extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blockModalWindow: false,
      openCategories: false,
      events: []
    }
  }

  getEvents() {
    fetch('http://localhost:8000/events-api/')
        .then(results => results.json())
        .then(results => this.setState({events: results}))
        .catch(error => console.error(error));
  }

  componentDidMount() {
    this.getEvents();
  }




  blockWindow = () => {this.setState({blockModalWindow: this.state.blockModalWindow = true})}
  unlockWindow = () => {this.setState({blockModalWindow: this.state.blockModalWindow = false})}
  getBlockWindow = () => {return this.state.blockModalWindow}
  showCategories = () => {
    this.setState({openCategories: !this.state.openCategories})
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.openCategories != prevState.openCategories){
      if (this.state.openCategories == true){
          document.getElementsByClassName("categories-container").item(0).classList.add("showCategories");
          document.getElementsByClassName("events").item(0).classList.add("content_active");
      }
      else{
        document.getElementsByClassName("categories-container").item(0).classList.remove("showCategories");
        document.getElementsByClassName("events").item(0).classList.remove("content_active");
      }

    }

  }

  render() {

    return (
      <div className="content">
        <div className="menu-icon" onClick={this.showCategories}></div>
        <div className="events-container">
          <Categories app={this} />
          <div className="events">
            {this.state.events.map((x, index)=> <EventCard event={x} parentClass={this} index={index} />)}
          </div>
      </div>
      </div>
  );
  }
}
export default Events;