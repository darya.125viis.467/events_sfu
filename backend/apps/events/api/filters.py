from django.db import models as django_models
import django_filters
from apps.events.models import Events


class EventFilter(django_filters.FilterSet):
    class Meta:
        model = Events
        fields = {
            'date': ('lte', 'gte')
        }

    filter_overrides = {
        django_models.DateTimeField: {
            'filter_class': django_filters.IsoDateTimeFilter
        },
    }
