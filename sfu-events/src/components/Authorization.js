import React, { Component } from 'react';
import styles from '../styles/styles.css';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import AuthService from "../services/auth.service";
import {email, required, vpassword} from "../helpers/validators";
import {get_errors} from "../helpers/errors-processing";

class Authorization extends Component {
  constructor (props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.state = {
      email: "",
      password: "",
      loading: false,
      message: "",
    }
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleLogin(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    this.form.validateAll();


     if (this.checkBtn.context._errors.length === 0) {
          AuthService.login(this.state.email, this.state.password).then(
            () => {
               this.props.history.push("/");
               window.location.reload();
            },
            error => {
              const error_dict = error.response.data;
              let values = get_errors(error_dict)

              this.setState({
                  loading: false,
                  message: values
              });
            }
          );
        } else {
          this.setState({
            loading: false
          });
        }
  }

  render() {
    return (
      <div>
        <img src="../images/background.png" className="background-image">

        </img>
        <Form className="form-authorization"
              onSubmit={this.handleLogin}
              ref={c => {
                this.form = c;
              }}>
          <Input  className="input-login"
                 type="email"
                 name="email"
                 placeholder="Логин"
                 value={this.state.email}
                 onChange={this.onChangeEmail}
                 validations={[required, email]}>
          </Input>
          <Input  className="input-password"
                 type="password"
                 name="password"
                 placeholder="Пароль"
                 value={this.state.password}
                 onChange={this.onChangePassword}
                 validations={[required, vpassword]}
                 minlength="8">
          </Input>
          <div className="buttons">
            <button className="sign-in" disabled={this.state.loading}>Вход</button>
             {this.state.message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
            <a className="password-recovery" href="/reset">Забыли пароль?</a>
          </div>
        </Form>
      </div>
    );
  }
} export default Authorization;
