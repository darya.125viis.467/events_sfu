import React, { Component } from 'react';
import styles from '../styles/styles.css';

function Footer() {
  return(
    <div className="footer">
      <div className="footer-text">
        <p>© Сибирский федеральный университет</p>
        <p className="email">По вопросам обращайтесь в службу поддержи:  
          <a href="mailto:service202020@gmail.com"> service202020@gmail.com</a>
        </p>
      </div>
    </div>
  );
}
export default Footer;