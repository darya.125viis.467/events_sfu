export const API_URL = "http://localhost:8000/";
export const USER_API_URL = API_URL + "users-api/";
export const EVENTS_API_URL = API_URL + "events-api/";
export const CALENDAR_API_URL = API_URL + "calendar-api/";
