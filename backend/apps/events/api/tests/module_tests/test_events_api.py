from django.test import RequestFactory
from django.urls import reverse
from django.utils import timezone
from rest_framework.test import APITestCase, APIClient
from rest_framework import status

from apps.events.api.exceptions import ReadOnlyError
from apps.events.api.serializers import EventSerializer
from apps.events.models import Event, EventPostImage, EventPost
from apps.events.tests.factories import EventFactory, EventPostImageFactory
from apps.users.models import User

BASE_EVENTS_API_URL = '/events-api/'


class EventsAPITestCase(APITestCase):
    NUM_OF_EVENTS = 5

    def _assert_status_code(self, excepted, given, response):
        self.assertEqual(
            excepted, given,
            f'Excepted {excepted} but {given} was given\n'
            f'response data {response.data}'
        )

    def _assert_exception_code(self, excepted, given):
        self.assertEqual(excepted, given, f'Excepted {excepted} code, but {given} was given')

    @classmethod
    def setUpTestData(cls):
        cls.request = RequestFactory().request()

    def setUp(self):
        """Creates events, posts and images"""
        EventPostImageFactory.create_batch(self.NUM_OF_EVENTS)
        self.event_post_images = EventPostImage.objects.all()
        self.event_posts = EventPost.objects.all()
        self.events = Event.objects.all()
        self.api_client = APIClient()

    def tearDown(self):
        pass

    def test_get_all_events(self):
        serializer = EventSerializer(self.events, many=True, context={'request': self.request})
        response = self.api_client.get(BASE_EVENTS_API_URL)
        self._assert_status_code(status.HTTP_200_OK, response.status_code, response)
        self.assertEqual(serializer.data, response.data)

    def test_get_one_event(self):
        event = self.events[0]
        serializer = EventSerializer(event, context={'request': self.request})
        response = self.api_client.get(BASE_EVENTS_API_URL + f'{event.id}/')
        self._assert_status_code(status.HTTP_200_OK, response.status_code, response)
        self.assertEqual(serializer.data, response.data)

    def test_create_event(self):
        user = User.objects.all()[0]
        self.api_client.force_login(user)
        new_event_data = {
            'title': 'new_title',
            'start': str(timezone.now() + timezone.timedelta(1)),
            'description': 'some_desc',
            'place': '',
        }
        response = self.api_client.post(
            BASE_EVENTS_API_URL,
            data=new_event_data,
            format='multipart',
        )
        self._assert_status_code(status.HTTP_201_CREATED, response.status_code, response)

    def test_change_event_fields(self):
        event = self.events[0]
        pk = event.id
        with self.subTest('Change title'):
            data = {'title': 'new_title'}
            response = self.api_client.patch(BASE_EVENTS_API_URL+f'{pk}/', data, format='multipart')
            self._assert_status_code(status.HTTP_200_OK, response.status_code, response)
            event = self.events.get(id=pk)
            self.assertEqual(event.title, data['title'])
        with self.subTest('Change place'):
            data = {'place': 'new_place'}
            response = self.api_client.patch(BASE_EVENTS_API_URL+f'{pk}/', data, format='multipart')
            self._assert_status_code(status.HTTP_200_OK, response.status_code, response)
            event = self.events.get(id=pk)
            self.assertEqual(event.place, data['place'])
        with self.subTest('Change description'):
            data = {'description': 'new_description'}
            response = self.api_client.patch(BASE_EVENTS_API_URL+f'{pk}/', data, format='multipart')
            self._assert_status_code(status.HTTP_200_OK, response.status_code, response)
            event = self.events.get(id=pk)
            self.assertEqual(event.description, data['description'])
        with self.subTest('Change start'):
            data = {'start': timezone.now() + timezone.timedelta(1)}
            response = self.api_client.patch(BASE_EVENTS_API_URL+f'{pk}/', data, format='multipart')
            self._assert_status_code(status.HTTP_200_OK, response.status_code, response)
            event = self.events.get(id=pk)
            self.assertEqual(event.start, data['start'])
        with self.subTest('Change owner(musn\'t work)'):
            data = {'owner': pk+1}
            response = self.api_client.patch(BASE_EVENTS_API_URL+f'{pk}/', data, format='multipart')
            self._assert_status_code(status.HTTP_400_BAD_REQUEST, response.status_code, response)
            self._assert_exception_code(ReadOnlyError.default_code, response.data['detail'].code)
        with self.subTest('Change created(musn\'t work)'):
            data = {'created': timezone.now() - timezone.timedelta(1)}
            response = self.api_client.patch(BASE_EVENTS_API_URL+f'{pk}/', data, format='multipart')
            self._assert_status_code(status.HTTP_400_BAD_REQUEST, response.status_code, response)
            self._assert_exception_code(ReadOnlyError.default_code, response.data['detail'].code)

    def test_delete_event(self):
        event = self.events[0]
        response = self.api_client.delete(BASE_EVENTS_API_URL + f'{event.id}/')
        self._assert_status_code(status.HTTP_204_NO_CONTENT, response.status_code, response)
