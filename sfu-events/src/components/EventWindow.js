import React, { Component } from 'react';
import UserService from '../services/user.service'
import styles from '../styles/styles.css';

class EventWindow extends Component {
  constructor(props) {
    super(props);
    this.addToActual = this.addToActual.bind(this);
    this.removeFromActual = this.removeFromActual.bind(this);
    this.renderActualButton = this.renderActualButton.bind(this);
    this.state = {
      alreadyAdded: false,
      message: '',
    }
  }

  addToActual(){
    let event_id = this.props.event.id;
    UserService.addEventToActual(event_id)
        .then(result => this.setState({alreadyAdded: true}))
  }

  removeFromActual(){
    let event_id = this.props.event.id;
    UserService.removeEventFromActual(event_id)
        .then(result => this.setState({alreadyAdded: false}))
  }

  images(){
    let img = new Image();
    img.src = this.props.event.image;

    if ( img.naturalWidth< img.naturalHeight){

      return(<div className="window-img-container"><img src={this.props.event.image}
                                                        className="window-img-height" alt=""></img></div>);

    }
    else {
      return (<div ><img src={this.props.event.image}
                                                         className="window-img" alt=""></img></div>);
    }
  }

  checkIsAlreadyAdded() {
    UserService.isUserAlreadyJoined(this.props.event.id)
        .then(result => this.setState({alreadyAdded: result}))
  }

  componentDidMount() {
    this.checkIsAlreadyAdded();
  }

  renderActualButton(){
    if (this.state.alreadyAdded) {
        return (<button id='add_to_actual_button' className="window-button"
                onClick={this.removeFromActual}>Уже в актуальном</button>)
    }else {
        return (<button id='add_to_actual_button' className="window-button"
                  onClick={this.addToActual}>Добавить в актуальное</button>)
    }
  }

  render() {

  return (
    <div>
      <div className="modal-window">
        <span className="close" onClick={() => {this.props.close.closeWindow()}}></span>
        <div className="window-content">
          {/*<img src={this.props.images} className="window-img"></img>*/}
          {this.images()}
          <div className="window-scrolling-content">
            <div className="window-text">
              <div className="window-header">
                <div className="window-header-text">
                  <p><span>Название: </span>{this.props.event.name}</p>
                  <p><span>Дата: </span>{this.props.event.date}</p>
                  <p><span>Организация: </span>{this.props.event.organization.name}</p>
                  <p><span>Место проведения: </span>{this.props.event.location}</p>
                </div>
                {this.renderActualButton()}
                <div className="alert alert-danger" role="alert">
                  {this.state.message}
                </div>
              </div>
              <div className="window-body">
                {this.props.event.description}
              </div>
              <div className="window-footer">
                <p><span>Оставить отзыв: </span><a href={this.props.event.review}>{this.props.event.review}</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
     <div className="overlay"></div>
    </div>
  );
  }
}
export default EventWindow;