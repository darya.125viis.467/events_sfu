from django.contrib import admin

from .forms import OrganizationForm
from .models import Events, Organization, OrganizationAndActivity, Activity



@admin.register(Events)
class EventAdmin(admin.ModelAdmin):
    readonly_fields = ['unique_folder_name']
    search_fields = ('name',)
    list_display = (
        'id',
        'owner',
        'name',
        'organization',
        'date',
        'location',
    )
    list_display_links = ('name',)
    list_filter = (
        'organization',
    )
    ordering = ('name',)


@admin.register(Organization)
class EventAdmin(admin.ModelAdmin):
    form = OrganizationForm
    search_fields = ('name', 'institute')
    filter_horizontal = ('activities',)
    list_display = (
        'id',
        'name',
        'institute',
    )
    list_display_links = ('name',)
    list_filter = (
        'name',
        'institute',
    )
    ordering = ('name',)
    pass


@admin.register(OrganizationAndActivity)
class EventAdmin(admin.ModelAdmin):
    search_fields = ('organization',)
    list_display = (
        'id',
        'organization',
        'activity',
    )
    list_display_links = ('organization',)
    list_filter = (
        'organization',
        'activity',
    )
    ordering = ('organization',)
    pass


@admin.register(Activity)
class EventAdmin(admin.ModelAdmin):
    pass
