import React, {Component} from 'react';
import EventWindow from './EventWindow';
import Modal from './Modal'
import styles from '../styles/styles.css';
import Footer from "./Footer";
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

class EventCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            img: ""
        }
    }

    openWindow = () => {
        if (this.props.parentClass.getBlockWindow() === false) {
            //document.body.classList.add('modal-open');
            //document.getElementById('header').classList.add('max-header');
            this.setState({isOpen: true});
            this.props.parentClass.blockWindow();
        }
    }

    closeWindow = () => {
        //document.body.classList.remove('modal-open');
        //document.getElementById('header').classList.remove('max-header');
        this.setState({isOpen: false});
        this.props.parentClass.unlockWindow();
    }


    render() {
        let height = 10;
        let width = 0;
        let img = new Image();
        let index = this.props.index;
        img.src = this.props.event.image;
        img.onload = function () {

            height = this.height;
            width = this.width;

            if (width < height) {
                img.classList.add("card-image-height")

            } else {
                img.classList.add("card-image-width")
            }

            let div = document.getElementsByClassName("card-image-container").item(index);
            div.innerHTML = img.outerHTML;
        }
        return (
            <div className="event-card" key={this.props.event.id}>
                <div className="card-image-container"> {ReactHtmlParser(this.state.img)}</div>
                <div className="card-text">
                    <div className="card-title">{this.props.event.name}</div>
                    <div className="card-info">{this.props.event.description}</div>
                </div>
                <button className="card-button" onClick={this.openWindow}>Подробнее</button>
                <div>
                    {(this.state.isOpen && <Modal> <EventWindow event={this.props.event} close={this}/></Modal>)}
                </div>

            </div>

        );
    }
}

export default EventCard;