import pathlib
import uuid


def avatar_path(instance, filename):
    """Get unique filename with uuid.

    File will be uploaded to
    MEDIA_ROOT/profiles/avatars/unique_id.{<filename> extension}
    """
    file = pathlib.Path(filename)
    return f'profiles/avatars/{uuid.uuid4()}{file.suffix}'
