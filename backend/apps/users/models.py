import datetime
from urllib.parse import urljoin

from django.urls import reverse
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils.translation import ugettext_lazy as _

from .managers import UserManager

from apps.users.services import avatar_path

__all__ = (
    'User',
)


class User(AbstractBaseUser, PermissionsMixin):
    """Custom user model.

    Attributes:
        username (str):  Username.
        is_active (bool): Can user log in to the system.
        is_staff (bool): Can user access to admin interface.
        events (Manager): Many to many relationship for user events.

    Nested attributes:
        is_superuser (bool): The user can super access to admin UI.
        groups(Manager): The groups this user belongs to.
        user_permissions(Manager): Specified permissions for this user.
        last_login (datetime): Last date when user login to the system.

    """
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'

    username = models.CharField(
        max_length=255,
        default='Unnamed user',
        db_index=True,
        verbose_name=_("Username"),
        null=True
    )
    email = models.EmailField(
        max_length=255,
        unique=True,
        db_index=True,
        verbose_name=_("Email"),
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name=_("Is active"),
    )
    is_staff = models.BooleanField(
        default=False,
        verbose_name=_("Is staff"),
        help_text=_("The user will have access to admin interface."),
    )
    # Временная метка создания объекта.
    created_at = models.DateTimeField(
        auto_now_add=True,
    )
    # Временная метка показывающая время последнего обновления объекта.
    updated_at = models.DateTimeField(
        auto_now=True,
    )
    events = models.ManyToManyField(
        to='events.Events',
        related_name='users',
        verbose_name=_('Events'),
    )

    objects = UserManager()

    class Meta:
        db_table = 'users'
        ordering = ('email',)
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    def __str__(self):
        return f'{self.id} - {self.username} - {self.email}'

    def get_full_name(self):
        """
        Этот метод требуется Django для таких вещей, как обработка электронной
        почты. Обычно это имя фамилия пользователя, но поскольку мы не
        используем их, будем возвращать username.
        """
        return self.username

    def get_short_name(self):
        """ Аналогично методу get_full_name(). """
        return self.username

    def get_admin_change_url(self) -> str:
        """Get admin change URL.

        Build full url (host + path) to standard Django admin page for
        object like:

            https://api.sitename.com/admin/users/user/234/

        """

        assert self.id, "Instance must have an ID"

        return urljoin(
            settings.DJANGO_SITE_BASE_HOST,
            reverse('admin:users_user_change', args=(self.id,)),
        )
