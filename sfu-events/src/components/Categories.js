import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import styles from '../styles/styles.css';

class Categories extends Component {
    constructor(props) {
        super(props);
        this.state = {
            institutionsAreDropped: false,
            organizationsAreDropped: false,
            dormitoriesAreDropped: false,
            activitiesAreDropped: false,
            dateIsDropped: false,
            tagIsDropped: false
        }
    }

    showInstitutions = () => {
        this.setState({institutionsAreDropped: !this.state.institutionsAreDropped});
    }

    showOrganizations = () => {
        this.setState({organizationsAreDropped: !this.state.organizationsAreDropped});
    }

    showDormitories = () => {
        this.setState({dormitoriesAreDropped: !this.state.dormitoriesAreDropped});
    }

    showActivities = () => {
        this.setState({activitiesAreDropped: !this.state.activitiesAreDropped});
    }

    showDate = () => {
        this.setState({dateIsDropped: !this.state.dateIsDropped});
    }

    showTag = () => {
        this.setState({tagIsDropped: !this.state.tagIsDropped});
    }


    check = () => {

        let organizationInputList = document.getElementsByClassName("organizations").item(0)
            .getElementsByTagName("input");
        let institutionsInputList = document.getElementsByClassName("institutions").item(0)
            .getElementsByTagName("input");
        let activitiesInputList = document.getElementsByClassName("activities").item(0)
            .getElementsByTagName("input");
        let dormitoriesInputList = document.getElementsByClassName("dormitories").item(0)
            .getElementsByTagName("input");

        let obj = {
            organizations: [],
            institutions: [],
            activities: []
        };

        for (let i of organizationInputList) {
            if (i.checked) {
                obj["organizations"].push(i.value);
            }

        }


        for (let i of institutionsInputList) {
            if (i.checked) {
                obj["institutions"].push(i.value);
            }

        }

        for (let i of activitiesInputList) {
            if (i.checked) {
                obj["activities"].push(i.value);
            }

        }

        for (let i of dormitoriesInputList) {
            if (i.checked) {
                obj["organizations"].push(i.value);
            }

        }

        let body = JSON.stringify(obj);

        const app = this.props.app

        fetch('http://localhost:8000/events-api/get_filtered_events/', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: body
        })
            .then(results => results.json())
            .then(function (results){
                app.setState({events: results});
            })
    }

    render() {
        var classNameForInstitut = this.state.institutionsAreDropped ? "menu-item isDropped" : "menu-item isHidden";
        var classNameForOrganization = this.state.organizationsAreDropped ? "menu-item isDropped" : "menu-item isHidden";
        var classNameForDormitory = this.state.dormitoriesAreDropped ? "menu-item isDropped" : "menu-item isHidden";
        var classNameForActivity = this.state.activitiesAreDropped ? "menu-item isDropped" : "menu-item isHidden";
        var classNameForDate = this.state.dateIsDropped ? "input-date isDropped" : "input-date isHidden";
        var classNameForTag = this.state.tagIsDropped ? "input-tag isDropped" : "input-tag isHidden";
        return (
            <div className={"categories-container"}>
                <div className="categories">
                    <a onClick={this.showActivities}>Вид деятельности</a>
                    <ul className="menu activities">
                        <li className={classNameForActivity}>
                            <input type="checkbox" value="учебная" onChange={this.check}/>
                            <label>учебная</label>
                        </li>
                        <li className={classNameForActivity}>
                            <input type="checkbox" value="спортивная" onChange={this.check}/>
                            <label>спортивная</label>
                        </li>
                        <li className={classNameForActivity}>
                            <input type="checkbox" value="научно-исследовательская" onChange={this.check}/>
                            <label>научно-исследовательская</label>
                        </li>
                        <li className={classNameForActivity}>
                            <input type="checkbox" value="общественная" onChange={this.check}/>
                            <label>общественная</label>
                        </li>
                    </ul>

                    <a onClick={this.showDormitories}>Общежитие</a>
                    <ul className="menu dormitories">
                        <li className={classNameForDormitory}>
                            <input type="checkbox" value="Общежитие СФУ №5" onChange={this.check}/>
                            <label>Общежитие СФУ №5</label>
                        </li>
                        <li className={classNameForDormitory}>
                            <input type="checkbox" value="Общежитие СФУ №6" onChange={this.check}/>
                            <label>Общежитие СФУ №6</label>
                        </li>
                        <li className={classNameForDormitory}>
                            <input type="checkbox" value="Общежитие СФУ №21 (Tesla Village)" onChange={this.check}/>
                            <label>Общежитие СФУ №21 (Tesla Village)</label>
                        </li>
                        <li className={classNameForDormitory}>
                            <input type="checkbox" value="Общежитие СФУ №22" onChange={this.check}/>
                            <label>Общежитие СФУ №22</label>
                        </li>
                        <li className={classNameForDormitory}>
                            <input type="checkbox" value="Общежитие СФУ №30" onChange={this.check}/>
                            <label>Общежитие СФУ №30</label>
                        </li>
                    </ul>

                    <a onClick={this.showOrganizations}>Организация</a>
                    <ul className="menu organizations">
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Microsoft IT Academy ИКИТ"
                                   onChange={this.check}/>
                            <label>Microsoft IT Academy ИКИТ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Абитуриент ИКИТ СФУ" onChange={this.check}/>
                            <label>Абитуриент ИКИТ СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Антикоррупционный студенческий клуб"
                                   onChange={this.check}/>
                            <label>Антикоррупционный студенческий клуб</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Беги за мной! СФУ" onChange={this.check}/>
                            <label>Беги за мной! СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="вОбщежитии СФУ" onChange={this.check}/>
                            <label>вОбщежитии СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Вокально-инструментальный ансамбль СФУ ПИ 'Интервал'"
                                   onChange={this.check}/>
                            <label>Вокально-инструментальный ансамбль СФУ ПИ 'Интервал'</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Волонтерский центр СФУ" onChange={this.check}/>
                            <label>Волонтерский центр СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Информер ИКИТ СФУ" onChange={this.check}/>
                            <label>Информер ИКИТ СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Информер ПИ СФУ<" onChange={this.check}/>
                            <label>Информер ПИ СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Клуб интеллектуальных игр СФУ"
                                   onChange={this.check}/>
                            <label>Клуб интеллектуальных игр СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Корейский язык в СФУ" onChange={this.check}/>
                            <label>Корейский язык в СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="МЦ ИКИТ СФУ" onChange={this.check}/>
                            <label>МЦ ИКИТ СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Научная библиотека СФУ" onChange={this.check}/>
                            <label>Научная библиотека СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Научный клуб МЦ ИКИТ" onChange={this.check}/>
                            <label>Научный клуб МЦ ИКИТ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Новости СФУ" onChange={this.check}/>
                            <label>Новости СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="НОЦ Молодых ученых СФУ" onChange={this.check}/>
                            <label>НОЦ Молодых ученых СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="ПОС ИКИТ (профсоюз)" onChange={this.check}/>
                            <label>ПОС ИКИТ (профсоюз)</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="ПОС ПИ СФУ" onChange={this.check}/>
                            <label>ПОС ПИ СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="ППОС СФУ (Профком студентов)"
                                   onChange={this.check}/>
                            <label>ППОС СФУ (Профком студентов)</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Психологическая служба СФУ"
                                   onChange={this.check}/>
                            <label>Психологическая служба СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Рок-клуб СФУ" onChange={this.check}/>
                            <label>Рок-клуб СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Сибирский федеральный университет"
                                   onChange={this.check}/>
                            <label>Сибирский федеральный университет</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Совет обучающихся СФУ" onChange={this.check}/>
                            <label>Совет обучающихся СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Союз молодежи СФУ" onChange={this.check}/>
                            <label>Союз молодежи СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Студенческие отряды СФУ" onChange={this.check}/>
                            <label>Студенческие отряды СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Студенческий комитет" onChange={this.check}/>
                            <label>Студенческий комитет</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Учебно-организационный отдел ИКИТ"
                                   onChange={this.check}/>
                            <label>Учебно-организационный отдел ИКИТ</label>
                        </li>
                        <li
                            className={classNameForOrganization}>
                            <input type="checkbox" value="Французский центр СФУ" onChange={this.check}/>
                            <label>Французский центр СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Центр карьеры СФУ" onChange={this.check}/>
                            <label>Центр карьеры СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Центр студенческой культуры СФУ"
                                   onChange={this.check}/>
                            <label>Центр студенческой культуры СФУ</label>
                        </li>
                        <li className={classNameForOrganization}>
                            <input type="checkbox" value="Японский центр СФУ" onChange={this.check}/>
                            <label>Японский центр СФУ</label>
                        </li>
                    </ul>

                    <a onClick={this.showInstitutions}>Институт</a>
                    <ul className="menu institutions">
                        <li className={classNameForInstitut}>
                            <input type="checkbox" value="ИКИТ" onChange={this.check}/>
                            <label>ИКИТ</label>
                        </li>
                        <li className={classNameForInstitut}>
                            <input type="checkbox" value="ПИ" onChange={this.check}/>
                            <label>ПИ</label>
                        </li>
                        <li className={classNameForInstitut}>
                            <input type="checkbox" value="СФУ" onChange={this.check}/>
                            <label>СФУ</label>
                        </li>
                    </ul>

                    <div>
                        <a onClick={this.showTag}>Тег</a>
                        <input type="text" className="input-tag" className={classNameForTag}></input>
                    </div>

                    <div>
                        <a onClick={this.showDate}>Дата</a>
                        <input type="date" className="input-date" className={classNameForDate}></input>
                    </div>
                </div>
            </div>
        );
    }
}

export default Categories;