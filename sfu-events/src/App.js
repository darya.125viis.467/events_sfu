import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Route, BrowserRouter} from "react-router-dom";
import Header from './components/Header';
import Events from './components/Events';
import Footer from './components/Footer';
import Authorization from './components/Authorization';
import Registration from './components/Registration';
import Calendar from './components/Calendar';
import PasswordReset from './components/PasswordReset';
import styles from './styles/styles.css';
import PasswordResetConfirm from "./components/PasswordResetConfirm";

class App extends Component {
  constructor (props) {
    super(props);
    this.state = {
      signButton: true,
      contentIsLoad: false
    }
  }



  getSignButton = () => {
    return this.state.signButton;
  }


  setSignButtonFalse = () => {
    this.setState({signButton: this.state.signButton = false})
  }

  setSignButtonTrue = () => {
    this.setState({signButton: this.state.signButton = true})
  } 




    render() {
    return(
      <BrowserRouter >
          <div className="App">
            <Route exact path="/" component={()=> (<Header hideButton = {true} logout={this}/>)}/>
            <Route exact path="/" component = { () => 
              (<div className="container">
                <Events app={this}  id={'app-root'}/>
                <Footer />
              </div>)}/>

            <Route exact path="/signin" component={()=> (<Header hideButton = {false} logout={this}/>)}/>
            <Route exact path="/signin" component ={Authorization} />
            <Route exact path="/signup" component={()=> (<Header hideButton = {false} logout={this}/>)}/>
            <Route exact path="/signup" component = {Registration} />

            <Route exact path="/reset" component={()=> (<Header hideButton = {false} logout={this}/>)}/>
            <Route exact path="/reset" component = {PasswordReset} />

            <Route exact path="/reset/confirm/" component={()=> (<Header hideButton = {false} logout={this}/>)}/>
            <Route path="/reset/confirm/" component={PasswordResetConfirm}/>

            <Route exact path="/calendar" component={()=> (<Header hideButton = {true} logout={this}/>)}/>
            <Route exact path="/calendar" component ={Calendar} />
          </div>
        </BrowserRouter>
    );
  }
}

export default App;
