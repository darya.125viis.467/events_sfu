import React, { Component } from 'react';
import  {NavLink, Redirect} from "react-router-dom";
import styles from '../styles/styles.css';
import logotype from '../images/logo.jpg';
import enter from '../images/enter.png';
import user from '../images/user.png'
import UserService from "../services/user.service";
import AuthService from "../services/auth.service";
import authHeader from '../services/auth-header';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      logout: false
    }
  }

  getUserInfo() {
    if (authHeader()) {
      UserService.getUserInfo()
          .then(result => this.setState({"userName": result.data.email}))
          .catch(result => this.setState({"userName": ""}))
    }
  }
     
  deleteCookie = () => {
    AuthService.logout()
    this.setState({"userName": ""})
  }

  componentDidMount() {
    this.getUserInfo();
  }


  render() {
    return(
      <header id="header">
        <div className="header-content">
          <div className="logo">
            <NavLink  to="/" className="logo-btn"><img className="logo-img" src={logotype} alt="logotype"></img></NavLink>
            <h1 className="logo-text">Мероприятия СФУ</h1>
          </div>

          { this.state.userName ?
            <nav className="header-nav"> 
              <img className="enter-icon" src={user} alt="User icon" ></img>
              <ul className="nav">
                <li className="nav-item"><NavLink to="/calendar" className="nav-item-link">Календарь</NavLink></li>
                <li><NavLink to="/" className="nav-item-link" onClick={this.deleteCookie}>Выход</NavLink></li>
              </ul>
            </nav>
           :this.props.hideButton &&
          <nav className="header-nav"> 
            <img src={enter} className="enter-icon" alt="enter icon"></img>
            <ul className="nav">
              <li className="nav-item"><NavLink to="/signin" className="nav-item-link">Вход</NavLink></li>
              <li><NavLink  to="/signup" className="nav-item-link">Регистрация</NavLink></li>
            </ul>
          </nav>
          }
        </div>
      </header>

    );
  }
}
export default Header;