import {isEmail} from "validator";
import React from "react";

export const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Это поле не должно быть пустым!
      </div>
    );
  }
};

export const email = value => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        Данная строка не является почтой.
      </div>
    );
  }
};

export const vpassword = value => {
  if (value.length < 8 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
        Длина пароля должна быть от 8 до 40 символов.
      </div>
    );
  }
};

export const vusername = value => {
  if (value.length < 3 || value.length > 20) {
    return (
      <div className="alert alert-danger" role="alert">
        Длина имени пользователя должна быть от 3 до 20 символов.
      </div>
    );
  }
};

export const vconfirm = (value, props, components) => {
   if (value !== components['password'][0].value) {
    return <div className="alert alert-danger" role="alert">
        Пароли не совпадают.
    </div>
  }
};