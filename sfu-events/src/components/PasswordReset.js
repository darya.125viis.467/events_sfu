import React, { Component } from 'react';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import PasswordResetService from "../services/password-reset";
import {required} from "../helpers/validators";
import {get_errors} from "../helpers/errors-processing";

class PasswordReset extends Component {
  constructor (props) {
    super(props);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.state = {
      email: "",
      loading: false,
      message: "",
      successful: false,
    }
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  handleReset(e) {
      e.preventDefault();
      this.form.validateAll();
      this.setState({
          message: "",
          loading: true,
          successful: false,
      });

      if (this.checkBtn.context._errors.length === 0) {
          PasswordResetService.resetResponse(this.state.email).then(
              response => {
                  this.setState({
                    loading: false,
                    message: response.success,
                    successful: true,
                  });
                },
              error => {
                const error_dict = error.response.data;
                let values = get_errors(error_dict)

                this.setState({
                    loading: false,
                    message: values
                });
              }
          );
      }else {
          this.setState({
            loading: false
          });
      }
  }

  render() {
    return (
      <div>
        <img src="../images/background.png" className="background-image">

        </img>
        <Form className="form-reset"
              onSubmit={this.handleReset}
              ref={c => {
                this.form = c;
              }}>
          <Input className="input-login"
                 type="text"
                 name="email"
                 placeholder="Введите email"
                 value={this.state.email}
                 onChange={this.onChangeEmail}
                 validations={[required]}>
          </Input>
          <div className="buttons">
            <button className="sign-in" disabled={this.state.loading}>Сменить пароль</button>
             {this.state.message && (
              <div className="form-group">
                <div
                  className={
                    this.state.successful
                      ? "alert alert-success"
                      : "alert alert-danger"
                  }
                  role="alert"
                >
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </div>
        </Form>
      </div>
    );
  }

} export default PasswordReset;