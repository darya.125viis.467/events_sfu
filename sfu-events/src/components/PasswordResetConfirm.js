import React, { Component } from 'react';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import PasswordResetService from "../services/password-reset";
import { parse } from 'query-string';
import {required, vpassword, vconfirm} from "../helpers/validators";
import {get_errors} from "../helpers/errors-processing";


class PasswordResetConfirm extends Component {
  constructor (props) {
    super(props);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onChangeConfirm = this.onChangeConfirm.bind(this);
    this.handleReset = this.handleReset.bind(this);
    const { location: { search } } = this.props;
    this.params = parse(search);
    this.state = {
      password: "",
      confirm: "",
      loading: false,
      message: "",
      successful: false,
    }
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  onChangeConfirm(e) {
    this.setState({
      confirm: e.target.value
    });
  }

  handleReset(e) {
      e.preventDefault();

      this.form.validateAll();

      this.setState({
          message: "",
          loading: true,
      });

      if (this.checkBtn.context._errors.length === 0) {
          PasswordResetService.reset(
              this.params.uid,
              this.params.token,
              this.state.password,
              this.state.confirm,
          ).then(
              response => {
                  this.setState({
                      loading: false,
                      message: response.success,
                      successful: true,
                  });
              },
              error => {
                  if ("token" in error.response.data) {
                      this.setState({
                          loading: false,
                          successful: false,
                          message: "Ссылка для смены пароля больше не действительна",
                      });
                      return
                  }
                  let message = get_errors(error.response.data)
                  this.setState({
                      loading: false,
                      successful: false,
                      message: message,
                  });
              }
          );
      }else {
          this.setState({
            loading: false
          });
      }
  }

  render() {
    return (
      <div>
        <img src="../images/background.png" className="background-image">

        </img>
        <Form className="form-reset"
              onSubmit={this.handleReset}
              ref={c => {
                this.form = c;
              }}>
            <Input className="input-password"
                 type="password"
                 name="password"
                 placeholder="Пароль"
                 value={this.state.password}
                onChange={this.onChangePassword}
                validations={[required, vpassword]}>
            </Input>
            <Input className="input-password"
                 type="password"
                 name="confirm"
                 placeholder="Подтверждение пароля"
                 value={this.state.confirm}
                onChange={this.onChangeConfirm}
                validations={[required, vpassword, vconfirm]}>
            </Input>
          <div className="buttons">
            <button className="sign-in" disabled={this.state.loading}>Сменить пароль</button>
             {this.state.message && (
              <div className="form-group">
                <div
                  className={
                    this.state.successful
                      ? "alert alert-success"
                      : "alert alert-danger"
                  }
                  role="alert"
                >
                  {this.state.message}
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </div>
        </Form>
      </div>
    );
  }

} export default PasswordResetConfirm;