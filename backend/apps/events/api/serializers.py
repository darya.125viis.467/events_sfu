from rest_framework import serializers, status
from rest_framework.exceptions import APIException

from apps.events.models import Events, Organization
from apps.users.api.serializers import UserSerializer


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ['name']


class EventSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True)
    organization = OrganizationSerializer()

    class Meta:
        model = Events
        fields = [
            'id',
            'owner',
            'post_id',
            'name',
            'description',
            'date',
            'organization',
            'location',
            'review',
            'image'
        ]
        read_only_fields = ['id', 'owner']

    def create(self, validated_data):
        organization_name = validated_data.pop('organization', None)
        organization = Organization.objects.get(name=organization_name.get('name'))
        if not organization:
            raise APIException('event must have organization', code=status.HTTP_400_BAD_REQUEST)
        event = Events.objects.create(**validated_data, organization=organization)
        return event
