import axios from 'axios';
import authHeader from './auth-header';
import {USER_API_URL, EVENTS_API_URL, CALENDAR_API_URL} from "./constants";


class UserService {
  getUserInfo(){
      return axios.get(USER_API_URL + "user-info/", { headers: authHeader() })
  }

  getUsersEvents(start, end) {
    return axios.get(CALENDAR_API_URL, { headers: authHeader() , params: {date__gte: start, date__lte: end}});
  }

  addEventToActual(event_id) {
    return axios.patch(EVENTS_API_URL + event_id + "/join/", null, { headers: authHeader() });
  }

  removeEventFromActual(event_id) {
    return axios.delete(CALENDAR_API_URL + event_id,{ headers: authHeader() });
  }

  isUserAlreadyJoined(event_id){
    return axios.get(CALENDAR_API_URL + `${event_id}`, { headers: authHeader() })
        .then(function (result) {
          return true;
        })
        .catch(function (error) {
           return false;
        })
  }

}

export default new UserService();