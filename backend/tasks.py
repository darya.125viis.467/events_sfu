import os
import platform

from invoke import task

RUN_DOCKER_WITH_MANAGE = 'docker-compose run --rm backend python manage.py'
VK_SECRETS_PREFIX = 'EVENTS_VK_SECRET_'
ADMIN_SECRETS_PREFIX = 'EVENTS_ADMIN_SECRET_'


@task(help={'env': 'environment'})
def compiledeps(c, env):
    """Compile  dependencies file."""
    c.run(
        f'pip-compile ./requirements/{env}.in '
        f'-o ./requirements/{env}.txt '
        '-v'
    )


@task
def pep8(c, app=''):
    """Pep8 check."""
    c.run(
        f'flake8 ./apps/{app}'
    )


@task(name='setupgithooks')
def set_up_git_hooks(c):
    """Set up predefined hooks to the project's GIT repo."""
    c.run('git config core.hooksPath git-hooks')
    # make hooks executable
    c.run('chmod +x git-hooks/*')


@task(name='superuser')
def create_super_user(c, email="admin@localhost.com", password="123"):
    """Create superuser with password."""
    migrate(c)
    create_user_script = (
        f"from apps.users.models import User; "
        f"User.objects.create_superuser('{email}', '{password}')"
    )
    c.run(f'{RUN_DOCKER_WITH_MANAGE} shell -c "{create_user_script}"')

    print("Superuser created successfully.")


@task(name='run')
def run_docker(c):
    """Run docker."""
    c.run('docker-compose up -d')


@task(name='stop')
def stop_docker(c):
    """Stop docker."""
    c.run('docker-compose stop')


@task(name='destroy')
def destroy_docker(c):
    """Destroy docker."""
    c.run('docker-compose down')


@task(name='seed')
def seed_database(c):
    """Seed database."""
    c.run(f'{RUN_DOCKER_WITH_MANAGE} runscript seed_database')


@task(name='clear')
def clear_database(c):
    """Clear database data."""
    c.run(f'{RUN_DOCKER_WITH_MANAGE} reset_db --noinput --close-sessions')
    print("Cleared successfully.")


@task(stop_docker, name='build')
def build_docker(c):
    """Build docker."""
    c.run('docker-compose build')


@task(name='migrate')
def migrate(c):
    """Make and run migrations."""
    c.run(f'{RUN_DOCKER_WITH_MANAGE} makemigrations')
    c.run(f'{RUN_DOCKER_WITH_MANAGE} migrate')


@task(name='prepare')
def prepare_project(c, fresh=False, seed=False):
    """Prepare project with params (fresh, seed).
    
    Args:
        flag '--fresh' invoke a task to clear database.
        flag '--seed' invoke a task to seed database.
    """
    build_docker(c)
    run_docker(c)
    if fresh:
        clear_database(c)
        create_super_user(c)
    migrate(c)
    if seed:
        seed_database(c)


@task(name='test')
def run_tests(c):
    """Run all application tests."""
    c.run('docker-compose run backend pytest')


@task
def set_secrets(
        c,
        vk_login,
        vk_password,
        admin_email="admin@localhost.com",
        admin_password="123",
):
    """Create .env file with credentials"""
    update_vk_secrets(c, vk_login, vk_password)
    update_admin_secrets(c, admin_email, admin_password)
    print("Credentials specified")


@task
def update_admin_secrets(
        c,
        admin_email="admin@localhost.com",
        admin_password="123",
):
    if platform.system() != 'Windows':
        c.run('sudo rm -f .env.admin')
        os.environ[f"{ADMIN_SECRETS_PREFIX}ADMIN_EMAIL"] = admin_email
        os.environ[f"{ADMIN_SECRETS_PREFIX}ADMIN_PASSWORD"] = admin_password
        c.run(
            f"dump-env "
            f"-t .env.admin.template "
            f"-p '{ADMIN_SECRETS_PREFIX}' > .env.admin")
    else:
        with open('.env.admin', 'w') as f:
            f.writelines([
                f'ADMIN_EMAIL={admin_email}\n',
                f'ADMIN_PASSWORD={admin_password}',
            ])

    create_user_script = (
        f"from apps.users.models import User; "
        f"User.objects.create_superuser('{admin_email}', '{admin_password}')"
    )
    c.run(f'python manage.py shell -c "{create_user_script}"')
    print("Admin credentials updated")


@task
def update_vk_secrets(
        c,
        vk_login,
        vk_password,
):
    if platform.system() != 'Windows':
        c.run('sudo rm -f .env.admin')
        os.environ[f"{VK_SECRETS_PREFIX}VK_USERNAME"] = vk_login
        os.environ[f"{VK_SECRETS_PREFIX}VK_PASSWORD"] = vk_password
        c.run(
            f"dump-env "
            f"-t .env.vk.template "
            f" -p '{VK_SECRETS_PREFIX}' > .env.vk")
    else:
        with open('.env.vk', 'w') as f:
            f.writelines([
                f'VK_USERNAME={vk_login}\n',
                f'VK_PASSWORD={vk_password}',
            ])
    print("VK credentials updated")
