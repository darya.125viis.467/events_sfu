import factory
from django.utils import timezone


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'users.User'

    username = factory.Faker('name')
    email = factory.Faker('email')


class EventFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'events.Event'

    owner = factory.SubFactory(UserFactory)
    title = factory.Sequence(lambda n: f'event_title_{n}')
    start = timezone.now()
    description = 'description'


class EventPostFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'events.EventPost'

    event = factory.SubFactory(EventFactory)
    title = factory.Sequence(lambda n: f'event_post_title_{n}')


class EventPostImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'events.EventPostImage'

    event_post = factory.SubFactory(EventPostFactory)
    image = factory.django.ImageField(color='blue')
