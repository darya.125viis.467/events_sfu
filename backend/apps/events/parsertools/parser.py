import re
from itertools import chain
from datetime import datetime

import dateutil.parser
import vk_api
from django.conf import settings

from apps.events.api.serializers import EventSerializer
from apps.users.models import User

UNINTERESTING = set(chain(dateutil.parser.parserinfo.JUMP,
                          dateutil.parser.parserinfo.PERTAIN,
                          ['a']))


def _get_date(tokens):
    for end in range(len(tokens), 0, -1):
        region = tokens[:end]
        if all(token.isspace() or token in UNINTERESTING
               for token in region):
            continue
        text = ''.join(region)
        try:
            date = dateutil.parser.parse(text)
            return end, date
        except ValueError:
            pass
        except OverflowError:
            pass
        except TypeError:
            pass


def find_dates(text, max_tokens=50, allow_overlapping=False):
    tokens = list(filter(None, re.split(r'(\S+|\W+)', text)))
    skip_dates_ending_before = 0
    for start in range(len(tokens)):
        region = tokens[start:start + max_tokens]
        result = _get_date(region)
        if result is not None:
            end, date = result
            if allow_overlapping or end > skip_dates_ending_before:
                skip_dates_ending_before = end
                yield date


def parse(wall_name, wall_id, access_token: str):
    """ Пример получения всех постов со стены """

    """ VkTools.get_all позволяет получить все объекты со всех страниц.
        Соответственно get_all используется только если метод принимает
        параметры: count и offset.
        Например может использоваться для получения всех постов стены,
        всех диалогов, всех сообщений, etc.
        При использовании get_all сокращается количество запросов к API
        за счет метода execute в 25 раз.
        Например за раз со стены можно получить 100 * 25 = 2500, где
        100 - максимальное количество постов, которое можно получить за один
        запрос (обычно написано на странице с описанием метода)
    """

    tools = vk_api.VkTools(vk_api.VkApi(
        login=settings.SECRETS.get('VK_USERNAME', None),
        password=settings.SECRETS.get('VK_PASSWORD', None),
        token=access_token,
    ))

    user = User.objects.get(email=settings.SECRETS.get('ADMIN_EMAIL', None))
    wall = tools.get_all_iter('wall.get', 1, {'owner_id': -wall_id})
    dict_list = []
    i = 0

    check = 0
    for item in wall:

        dict = {}

        # if not re.match(r'#анонс@chgk_sfu', item['text']):
        #     check += 1
        #     if check == 15:
        #         break
        #     continue
        # import ipdb; ipdb.set_trace()
        if item.get('attachments', None) and item.get('text', None):
            if i <= 15:
                if item['text'].find(".") < item['text'].find("\n") and item[
                    'text'].find(".") != -1:
                    dict['name'] = item['text'][0:item['text'].find(".")]
                else:
                    dict['name'] = item['text'][0:item['text'].find("\n")]
                dict['description'] = item['text']

                item_date = item.get('date')
                if item_date:
                    dict['date'] = datetime.fromtimestamp(item_date)

                dict['organization'] = {'name': wall_name}
                dict['location'] = 'location'
                dict['review'] = 'review'

                dict['post_id'] = item['id']

                for attachment in item['attachments']:
                    if attachment['type'] == 'photo':
                        dict['image'] = attachment['photo']['sizes'][-1]['url']
                        break
                dict_list.append(dict)
                i = i + 1
            else:
                break

    for dict in dict_list:
        serializer = EventSerializer(data=dict)
        try:
            if serializer.is_valid(raise_exception=True):
                serializer.save(owner=user)
        except Exception as e:
            print(e)
