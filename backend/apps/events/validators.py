from django.utils.translation import ugettext_lazy as _

from django.core.exceptions import ValidationError
from django.utils import timezone


class LessThanCreationDateException(ValidationError):
    pass


def events_start_date_validator(value):
    """Checks that the start date is not earlier than the creation date"""
    if value < timezone.now():
        raise LessThanCreationDateException(
            _('Start date cannot be less than creation date'),
            code='invalid',
        )
